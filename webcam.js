var port = 8000;

//webserver was modified version from github https://github.com/dwhitnee/tessel

var http = require('http');      // the interwebs
var url = require('url');

var tessel = require('tessel');
var camera = require('camera-vc0706').use(tessel.port['A']);

tessel.led[3].low;
var activeLED  = tessel.led[1]; // Blue LED, got a request
var readyLED   = tessel.led[0]; // Green LED, we are active


var Responses = {
  snapshot: function( response ) {
    
    //Take Picture and return filename
    response.end(SnapShot());

  }

};

var listener = function( request, response ) {

  console.log("Got a request to " + request.url );

  var req = url.parse( request.url, true );

  	if ((request.url === "/snapshot")){

    	Responses.snapshot( response );
    }
	else {
    	response.end();
  	} 

};

camera.on('ready', function() {
	readyLED.high();
});
camera.on('error', function(err) {
  console.error('Camera Error: ' + err);
});

var filename
var SnapShot = function(){
  activeLED.high();
	var time = Math.floor(Date.now());
	console.log("TimeStap: " + time)

	filename = 'image_' + time + '.jpg';
	 
	// Take the picture
	camera.takePicture(function(err, image) {
		
	    if (err) {
	    	console.log('error taking image', err);
	    } 
	    else {
		    // Save the image
		    console.log('Picture saving as', 'images/' + filename, '...');
		    process.sendfile('images/' + filename, image);
        activeLED.low();
	    }
	});	

	return 'images/' + filename;
}

var server = http.createServer( listener ).listen( port );
console.log("\nServer running at http://127.0.0.1:" + port);
console.log("");

