# README #


### What is this repository for? ###

A NodeJS server that will call into a Tessel API server request a picture be taken with Camera module.  Tessel save picture on NodeJS server and returns path to image.  NodeJS server then replies to initial web request with custom page that has link to picture taken by Tessel

NodeJS Webserver: server.js
Tessel API server: webcam.js

### Demo
You can access a live demo of the code at:
http://tessel.gametheorylabs.com:8080/snapshot

Note:  I am actively developing with Tessel, so this link may be down and change functionality during my development sessions.

### How do I get set up? ###

Update the IP address for Tessel in server.js

Tessel does not have to be accessible from internet, if NodeJS server and Tessel are on the same network.


If you have questions, contact me on Twitter @CoreyClarkPhD